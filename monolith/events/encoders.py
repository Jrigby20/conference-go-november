from .models import Location, Conference
from common.json import ModelEncoder


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "image_url",  # added after i added getphoto acl and portion to views
    ]

    def get_extra_data(self, o):
        return {
            "state": o.state.abbreviation
        }  # look at model encoder line 49 and 58-59


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder()
    }  # dont forget to initializae with ()


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]
