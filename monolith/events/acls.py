from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    """
    To get this one I had to add
    1. all this (pexels docs)
    2. add this function to view
    3. add image_url to model
    4. add image_url to encoder
    """
    url = f"http://api.pexels.com/v1/search?query={city}+{state}"

    headers = {"Authorization": PEXELS_API_KEY}

    resp = requests.get(url, headers=headers)
    data = resp.json()
    return data["photos"][1]["src"]["original"]


def get_weather_data(city, state):
    """
    to get this one I had to add
    1. all this (from docs)
    2. add this function to api views (detail view)
    3. no need to add model or encoder because you arent saving it but
    just adding to the output each time (check api view to see how)
    """
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"

    resp = requests.get(url)

    lat = resp.json()[0]["lat"]
    lon = resp.json()[0]["lon"]

    url_two = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"

    resp = requests.get(url_two)

    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"],
    }
